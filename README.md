## Installation

    git clone git@bitbucket.org:jonbuffington/vimdot.git ~/.vim
    cd ~/.vim
    mkdir -p ~/.vim/autoload && \
    curl -fLo ~/.vim/autoload/plug.vim https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    ln -s ~/.vim/vimrc ~/.vimrc

    vim
    ...
    :PlugInstall

## Install vim

    brew install vim

## vim-plug

The setup relies on [wim-plug](https://github.com/junegunn/vim-plug) to
cleanly install and manage vim plugins.

## Plugins

Plugins are added to the .vim/plugged directory using
vim-plug's `:PlugInstall`.

- [editorconfig-vim](https://github.com/editorconfig/editorconfig-vim.git)
- [fzf](https://github.com/junegunn/fzf)
- [fzf.vim](https://github.com/junegunn/fzf.vim)
- [nerdcommmenter](https://github.com/scrooloose/nerdcommenter)
- [papercolor-theme](https://github.com/NLKNguyen/papercolor-theme)
- [rust.vim](https://github.com/rust-lang/rust.vim)
- [syntastic](https://github.com/scrooloose/syntastic)
- [tagbar](https://github.com/majutsushi/tagbar)
- [vim-airline](https://github.com/bling/vim-airline)
- [vim-airline-themes](https://github.com/vim-airline/vim-airline-themes)
- [vim-easymotion](https://github.com/Lokaltog/vim-easymotion)
- [vim-fugitive](https://github.com/tpope/vim-fugitive)
- [vim-gitgutter](https://github.com/airblade/vim-gitgutter)
- [vim-markdown](https://github.com/tpope/vim-markdown)

Interesting plugins:

- [coc.nvim](https://github.com/neoclide/coc.nvim)
- [LeaderF](https://github.com/Yggdroot/LeaderF)
- [nerdtree](https://github.com/scrooloose/nerdtree)
